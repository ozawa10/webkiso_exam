<?php
//フォームで選んだ年月を表示、それ以外は現在の年月を表示
if(isset($_POST['yyyy']) && $_POST['yyyy'] !='' && isset($_POST['mm']) && $_POST['mm'] !=''){
 $year = $_POST['yyyy'];
 $month = $_POST['mm'];
}else{
 $year = date('Y');
 $month = date('m');
}
if(isset($_POST['start'])) {
 $start = $_POST['start'];
} else {
 $start = "日曜始め";  
}
 $dd = 1;
?>
<br>
<!--年月セレクト用フォーム-->
<form method="POST" action="<?php $_SERVER['PHP_SELF']; ?>">

<select name="yyyy">
<?php
for($i = 1990; $i <= 2030; $i++){
     echo '<option value="'.$i.'"'; if($i == $year) echo ' selected'; echo '>'.$i.'</option>'."\n";
}
?>
</select>年

<select name="mm">
<?php
for($i = 1; $i <= 12; $i++){
    echo '<option value="'.$i.'"'; if($i == $month) echo ' selected'; echo '>'.$i.'</option>'."\n";
}
?>
</select>月

<!--曜日初め用フォーム-->
<input name="start" type="radio" value="日曜始め" <?php if($start == '日曜始め'){ echo 'checked';} ?>>日曜始め
    <input name="start" type="radio" value="月曜始め" <?php if($start == '月曜始め'){ echo 'checked';} ?>>月曜始め
    <input type="submit" value="送信"><br>
<?php
if(isset($start) && ($start == '日曜始め' || $start == '月曜始め')){
  print "曜日：";
  print $start;
}else{
  print "曜日始めを選んで下さい。<br>" ;
}
?>  
</form>

<?php
//月末日を取得
$l_day = date('j',mktime(0,0,0,$month + 1,0,$year));

$calendar = array();
$j = 0;

//月末日までループ
for($i = 1; $i < $l_day + 1 ; $i++){
    //曜日を取得
    if($start == '日曜始め'){
       $week = date('w',mktime(0,0,0,$month,$i,$year));
    }else{
       $week = date('N',mktime(0,0,0,$month,$i,$year))-1; 
    }
       //1日の場合
       if($i == 1){
           //1日目の曜日までをループ
           for($s = 1; $s <= $week; $s++){
               //前半に空文字をセット
               $calendar[$j]['day'] = '';
               $j++;
           }
       }
       //配列に日付をセット
       $calendar[$j]['day'] = $i;
       $j++;
       //月末日の場合
       if($i == $l_day){
           //月末日から残りをループ
           for($e = 1; $e <= 6 - $week; $e++){
               //後半に空文字をセット
               $calendar[$j]['day'] = '';
               $j++;
           }
       }
    }
?>
<!--カレンダー表示-->
    <?php echo $year; ?>年<?php echo $month; ?>月のカレンダー
<br>
<br>
<table>
     <?php if($start == '日曜始め'){ ?>
    <tr>
        <th>日</th>
        <th>月</th>
        <th>火</th>
        <th>水</th>
        <th>木</th>
        <th>金</th>
        <th>土</th>
    </tr>
     <?php } else { ?>
    <tr>
        <th>月</th>
        <th>火</th>
        <th>水</th>
        <th>木</th>
        <th>金</th>
        <th>土</th>
        <th>日</th>
    </tr>
    <?php } ?> 
    <tr>
    <?php $cnt = 0; ?>
    <?php foreach ($calendar as $key => $value): ?>
 
        <td>
        <?php $cnt++; ?>
        <?php echo $value['day']; ?>
        </td>
 
    <?php if ($cnt == 7): ?>
    </tr>
    <tr>
    <?php $cnt = 0; ?>
    <?php endif; ?>
 
    <?php endforeach; ?>
    </tr>
</table>

<style type="text/css">
table{
        width:100%;
        font-family:cursive;
        font-size:18px;
        color:#00cccc;
     }
table th{
    background-color: #3366ff;
}
table th,
table td{
    border:1px solid #9999ff;
    text-align: center;
    padding: 10px;
}
    
</style>


